from __future__ import division, print_function
import os
import pandas as pd
import networkx as nx
import math


def get_nodes_from_df(df):
    """
    Gets a set of all nodes from an edges dataframe.

    Args:
        df (pd.DataFrame):

    Returns:
        set: Nodes of input.
    """
    s = set()
    s.update(df.source)
    s.update(df.target)

    return s


def load_edges(input_files, etypes, organisms, verbose):
    """
    Loads edges files and concatenates. Also returns a node-organism dictionary.
    
    Args:
        input_files:
        etypes: 
        organisms:
        verbose: 

    Returns:
        (pd.DataFrame, dict)
    """
    all_edges = []
    node_org_dict = {}

    for i, cur_file in enumerate(input_files):
        if verbose:
            print('Loading ' + cur_file)
        cur_df = pd.read_csv(cur_file, index_col=False, header=0, names=['source', 'target', 'value'])
        cur_df['file'] = cur_file
        if etypes:
            cur_df['type'] = etypes[i]
        if organisms:
            cur_df['organism'] = organisms[i]
            cur_nodes = get_nodes_from_df(cur_df)
            cur_nodes_dict = dict.fromkeys(cur_nodes, organisms[i])
            node_org_dict.update(cur_nodes_dict)
        all_edges.append(cur_df)

    if verbose:
        print('Concatenating')
    all_edges = pd.concat(all_edges).reset_index(drop=True)

    return all_edges, node_org_dict


def get_orthology(ortho_files, nodes):
    """
    Parse orthology input(s) into a dataframe. If nodes are given, it's used as filter.
    Also returns node-organism dictionary.

    Args:
        ortho_files (list)
        nodes (set): Only the orthology of these nodes (exclusive) will be added.

    Returns:
        (pd.DataFrame, dict):
    """
    node_org_dict = {}
    df = []
    for cur_file in ortho_files:
        new_df = pd.read_csv(cur_file)
        orgs = new_df.columns
        for cur_org in orgs:
            node_org_dict.update(dict.fromkeys(new_df[cur_org],
                                               cur_org))
        new_df.columns = ['source', 'target']
        new_df['type'] = 'Orthology'
        new_df['value'] = 1
        new_df['file'] = cur_file
        df.append(new_df)
    df = pd.concat(df).reset_index(drop=True)

    # FILTER
    if nodes:
        matches = df['source'].isin(nodes) & df['target'].isin(nodes)
        df = df[matches].reset_index(drop=True)

    return df, node_org_dict


def filter_df(df, nodes):
    """
    Exclusive nodes filter. Source and Target.
    Args:
        df (pd.DataFrame): 
        nodes:

    Returns:
        pd.DataFrame:
    """
    matches = df['source'].isin(nodes) & df['target'].isin(nodes)
    return df[matches].reset_index(drop=True)


def df_to_graph(df, use_pd_graph):
    """
    Converts a dataframe to a networkx graph.

    Args:
        use_pd_graph:
        df (pd.DataFrame):

    Returns:
        nx.MultiGraph
    """
    if use_pd_graph:
        G = nx.from_pandas_edgelist(df, 'source', 'target',
                                    edge_attr=True, create_using=nx.MultiGraph())
    else:
        G = nx.MultiGraph()
        for i, cur_row in df.iterrows():
            G.add_edge(cur_row.source, cur_row.target)
            for attribute in cur_row.index:
                G[cur_row.source][cur_row.target].update({attribute: cur_row[attribute]})

    return G


def check_output_folder_custom(folder):
    """
    Creates output folder if it does not exist.
    Args:
        folder:

    Returns:

    """
    if not os.path.exists(folder):
        os.makedirs(folder)


def write_graph_output_custom(g, extra_cols, folder):
    """
    Writes output.
    
    Args:
        g (nx.Graph): 
        extra_cols (list): 
        folder (string): 
    """
    check_output_folder_custom(folder)
    cols = ['source', 'target']
    for n in extra_cols:
        cols.append(n)

    fname = os.path.join(folder, 'node_list.csv')
    with open(fname, 'w') as f:
        for n in g.nodes():
            f.write(n + '\n')

    fname = os.path.join(folder, 'edge_list.csv')
    with open(fname, 'wb') as f:
        header = ','.join(cols) + '\n'
        f.write(header.encode('utf-8'))
        nx.write_edgelist(g, f,
                          delimiter=',',
                          data=extra_cols)


def write_graph_df_output_custom(df, extra_cols, folder):
    """
    Writes output.

    Args:
        df:
        extra_cols (list):
        folder (string): 
    """
    check_output_folder_custom(folder)
    cols = ['source', 'target']
    for n in extra_cols:
        cols.append(n)

    nodes = get_nodes_from_df(df)
    fname = os.path.join(folder, 'node_list.csv')
    with open(fname, 'w') as f:
        for n in nodes:
            f.write(n + '\n')

    fname = os.path.join(folder, 'edge_list.csv')
    df[cols].to_csv(fname, sep=',', index=False)


def write_graph_output(g, etypes, organisms, folder):
    """
    Writes networkx Graph edge and node list output.

    Args:
        organisms:
        etypes:
        g (nx.MultiGraph):
        folder (string): Destination.
    """
    fname = os.path.join(folder, 'node_list.csv')
    with open(fname, 'w') as f:
        for n in g.nodes():
            f.write(n + '\n')

    fname = os.path.join(folder, 'edge_list.csv')
    with open(fname, 'wb') as f:
        if etypes and organisms:
            f.write('source,target,type,organism,value,file\n'.encode('utf-8'))
            nx.write_edgelist(g, f,
                              delimiter=',', data=['type', 'organism', 'value', 'file'])
        elif etypes:
            f.write('source,target,type,value,file\n'.encode('utf-8'))
            nx.write_edgelist(g, f,
                              delimiter=',', data=['type', 'value', 'file'])
        elif organisms:
            f.write('source,target,organism,value,file\n'.encode('utf-8'))
            nx.write_edgelist(g, f,
                              delimiter=',', data=['organism', 'value', 'file'])
        else:
            f.write('source,target,value,file\n'.encode('utf-8'))
            nx.write_edgelist(g, f,
                              delimiter=',', data=['value', 'file'])

    return


def write_graph_df_output(df, etypes, organisms, folder):
    """
    Writes edges dataframe into edge and node list output.

    Args:
        organisms:
        etypes:
        df (pd.DataFrame):
        folder (string): Destination. 
    """
    all_nodes = set(df.source)
    all_nodes.update(df.target)

    fname = os.path.join(folder, 'node_list.csv')
    with open(fname, 'w') as f:
        for n in all_nodes:
            f.write(n + '\n')

    fname = os.path.join(folder, 'edge_list.csv')
    if etypes and organisms:
        cols = ['source', 'target', 'type', 'organism', 'value', 'file']
    elif etypes:
        cols = ['source', 'target', 'type', 'value', 'file']
    elif organisms:
        cols = ['source', 'target', 'organism', 'value', 'file']
    else:
        cols = ['source', 'target', 'value', 'file']
    df[cols].to_csv(fname, sep=',', index=False)

    return


def check_output_folder(cur_outfolder, cur_set, edge_type):
    """
    Names output folder, and makedirs if it doesn't exist.

    Args:
        cur_outfolder (string):
        cur_set (string):
        edge_type (string):

    Returns:
        string
    """
    if not edge_type:
        new_outfolder = os.path.join(cur_outfolder, cur_set)
    else:
        new_outfolder = os.path.join(cur_outfolder, edge_type, cur_set)
    if not os.path.exists(new_outfolder):
        os.makedirs(new_outfolder)

    return new_outfolder


def process_family(g, gene_org_dict, organisms):
    """
    Returns family nodes dictionary and code (organism count in input order: AT;SL;SL = [1 2]).
    
    Args:
        gene_org_dict:
        g (nx.Graph):
        organisms (list): 

    Returns:
        (dict, dict):
    """
    fam_dict = {}  # gene => family
    fam_code_dict = {}  # family => code = organisms count (order given in input) eg  [1 2 1]
    for cur_component in nx.connected_components(g):
        name = ';'.join(cur_component)

        cur_component_orgs = [gene_org_dict[cur_gene] for cur_gene in cur_component]
        fam_code_dict[name] = [cur_component_orgs.count(cur_org) for cur_org in organisms]

        for cur_gene in cur_component:
            fam_dict[cur_gene] = name

    return fam_dict, fam_code_dict


def get_ortho_groups(orthology_files):
    """
    Processes orthology files into dictionaries.
    Args:
        orthology_files (list):

    Returns:
        dict:
    """
    og_dict = {}   # gene => ortho_group
    for cur_ortho_df in orthology_files:
        cur_ortho_df.columns = ['source', 'target']
        for i, cur_row in cur_ortho_df.iterrows():
            for gene1, gene2 in [[cur_row.source, cur_row.target], [cur_row.target, cur_row.source]]:
                if gene1 not in og_dict:
                    og_name = ';'.join(sorted([gene1, gene2]))
                    og_dict[gene1] = og_name
                else:
                    og_name = og_dict[gene1]
                    if gene2 not in og_name:
                        og_members = og_name.split(';')
                        og_members.append(gene2)
                        new_og_name = ';'.join(sorted(og_members))
                        for cur_gene in og_members:
                            og_dict[cur_gene] = new_og_name

    return og_dict


def combinations(n, k):
    """
    Calculates number of possible k-combinations of n elements.
    Args:
        n:
        k:

    Returns:

    """
    if n == 1 or k > n:
        return 0
    if k == n:
        return 1
    else:
        num = math.factorial(n)
        den = math.factorial(n - k) * math.factorial(k)
        return num / den


def expected_score(fam1, fam2, fam_code_dict, all_organisms):
    """
    Calculates the expected score of two family nodes according to how many edges connect them, and how many could.
    Args:
        fam1:
        fam2:
        fam_code_dict:
        all_organisms:

    Returns:

    """
    fam1_code = fam_code_dict[fam1]
    fam2_code = fam_code_dict[fam2]

    expected = []
    for i in range(len(all_organisms)):
        # potential_edges - fam1_assumed_orth_edges - fam2_assumed_orth_edges
        expected.append(
                combinations(fam1_code[i] + fam2_code[i], 2) -
                combinations(fam1_code[i], 2) -
                combinations(fam2_code[i], 2)
        )

    return sum(expected)


def score_fam_network(G):
    """
    gets score dictionaries.
    
    Args:
        G (pf.Graph): 

    Returns:
        (dict, dict):
    """
    real_edge_score = {}            # edge => score
    real_edge_orgs = {}
    for cur_edge_data in G.edges(data=True):
        cur_edge = cur_edge_data[:2]
        if cur_edge not in real_edge_score:
            real_edge_score[cur_edge] = 1
        else:
            real_edge_score[cur_edge] += 1
        if cur_edge not in real_edge_orgs:
            real_edge_orgs[cur_edge] = {cur_edge_data[-1]['organism']}
        else:
            real_edge_orgs[cur_edge].add(cur_edge_data[-1]['organism'])
    return real_edge_score, real_edge_orgs
