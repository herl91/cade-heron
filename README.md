# CADE-HEroN
**C**omparative **A**nalysis of **D**ifferential **E**xpression, **H**omologs **E**xp**r**ession, C**o**expression **N**etworks

# OTHER REQUIREMENTS

To run "getDGE.R", the following needs to be installed:

* R, which can be downloaded from [their website](https://www.r-project.org/).
* The edgeR package for R, which can be found [here](https://bioconductor.org/packages/release/bioc/html/edgeR.html).

# EXAMPLE INPUT/OUTPUT
Examples of the input and output for this workflow can be found here:

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.4608913.svg)](https://doi.org/10.5281/zenodo.4608913)

# WORKFLOW
The initial input for this workflow is:

* At least 2 CSVs listing the coexpressed gene pairs in each species and their coexpression weight. The format for
these input files is as follows:

Species1:
```
source,target,value
geneX,geneY,0.7
geneY,geneZ,0.8
```
Species2:
```
source,target,value
geneP,geneQ,0.6
geneQ,geneR,0.8
```
* An orthology dictionary in CSV format listing with pairs of genes across all species that the script should consider
orthologous. The format for this is as follows:
```
ortholog_gene_1,ortholog_gene_2
geneX,geneZ
geneY,geneW
```
* A text file listing the genes that will be used as bait to target (one per line). The format for this file is as
follows:
```
geneX
geneW
```

# buildMultiNetwork.py
This is the main script of the workflow, which identifies the Logic Communities (LCs) and Orthologous Communities (OCs) showcased in the research
associated with these tools.

LCs are coexpression networks centered around a specific target gene. It shows all genes coexpressed with the target,
and also the coexpression among them.

OCs are coexpression networks centered around a specific target gene across multiple species. It shows all genes coexpressed with the target,
and also the coexpression among them. In this network, each node does not represent a gene, but instead is a family node,
representing a family of orthologous genes.

#### IMPORTANT ARGUMENTS
* --organisms: Comma-separated of the organisms of which input_file belongs to, in the same order. An example of submitting
an analysis of 2 species (rice and tomato) can be seen below:
```
buildMultiNetwork.py tomato_leaf_coexpression.csv rice_coexpression.csv tomato_root_coexpression.csv --organisms tomato,rice,tomato [OTHER ARGS]
```

* --lc: With this argument, the user can select the type of LCs that will be output by the tool. The following options are available:
    * none: No LCs will be output, only OCs.
    * uni: The output will include only LCs centered around one target each.
    * multi: The output will include only LCs involving 2 targets (and the genes coexpressed to both).
    * all: The output will include all LCs identified.

* ----untargeted: With this flag, the script will produce large networks of all family nodes that have some correlation.
These networks can be later analysed in Cytoscape. Two types of networks are output:
    * complex: In this network, two family nodes may be connected by multiple coexpression edges, each coming from a
    different input file (specified in the edge attributes).
    * simple: In this network, two family nodes may be connected by only ONE coexpression edge, the weight of which
    the number of "initial" coexpression links between the two family nodes, divided by the number of coexpression links
    the family _could_ have. For example, the analysis of species1 (geneX,geneY,geneZ) and species2 (gene1,gene2), may result
    in two family nodes "geneX;geneY;gene1" and "geneA;gene2", and the "simple" score associated with their correlation
    is 2/3 if only geneX-geneA and gene1-gene2 are correlated; alternatively, the score is 3/3 if geneY-geneA are also correlated.

#### OPTIONAL ARGUMENTS
* --etypes: One may provide different files for "different type" of correlation, for example: one list of gene pairs
coexpressed in leaf samples, and another input file for gene pairs coexpressed in root. This only needs to be used when
using the option directly below this one.
* --edge-type / -e: With this optional flag, the script will only find OCs with edge-types of the same type, so, for
for example, it will only associate root-coexpression of species1 with root-coexpression of species2, even if leaf
coexpression was also included in the input of both.

Example usage when analysing 2 coexpression types (leaf and root), across 3 species (tomato, rice, potato), and
requiring the analysis to be done for each tissue separately.
```
buildMultiNetwork.py tomato_leaf_coexpression.csv rice_leaf_coexpression.csv potato_leaf_coexpression.csv tomato_root_coexpression.csv rice_root_coexpression.csv --etypes leaf,leaf,leaf,root,root -e [OTHER ARGS]
```

* --exclusive / -x: With this flag, the edge-list of each community will be generated only with members of the community,
otherwise neighboring nodes are added to the network.
* --gene-info / -g: **RECOMMENDED** By including a CSV with additional annotation for each gene, the summary output can be annotated for
 easier analysing. Specifically, the annotation that can be added to each locus_tag is the gene_name or alias, and the
organism name. An example of the format of this file can be seen below:
```
Gene,plant,alias
gene1,rice,TX_Synthase
gene2,rice,DR_Synthase
```

#### OUTPUT
When using the "--gene-info" optional argument, the script will generate in the output folder a file summarizing the
results, called "full_summary.csv". This file lists all LCs and OCs identified within the analysis. For each comunity,
the following data is provided:

* c_type: Community type (LC/OC).
* edge: Edge type of this community.
* plant: Name(s) of organism(s) related to this community.
* alias: Gene alias of the target(s) around which this community was identified.
* bait: locus_tag of the target gene(s) around which this community was identified.
* count: Number of genes/family nodes in this community.
* code: ID of the community to (used to explore the results in the output folder).

A folder will be generated for LCs, and another for OCs. Each has a "summary.csv" listing the communities identified,
and the number of genes/family nodes in the community. One subfolder is generated for each community, each with two files:

* edge_list.csv: This file contains all the edges of the coexpression/orthology network generated by this community.
* node_list.csv: This file contains all the genes/family nodes in the coexpression/orthology network generated by this community.


# getDGE.R
This script generates a list of differentially expressed genes given a transcriptome in counts, and a file specifying the
samples to be compared.

#### INPUT
* counts_file: CSV of the transcriptome counts. One row per gene, one column per sample. Sample names must have a specific
format, described in the optional argument "--name-format".
* groups_file: CSV of the groups to be compared in thed differential analysis. The file must have no header, and must
list the treatment on the left, and the mock/control samples on the right. An example of the format can be seen below:
```
treatment1_timepoint1,mock1_timepoint1
treatment1_timepoint2,mock1_timepoint2
treatment2_timepoint1,mock2_timepoint1
```

#### IMPORTANT OPTIONAL ARGUMENTS
* --name-format: This script only support 3 naming formats for the sample names in the transcriptome. Each sample name
must have the treatment name and the replicate number separated by an underscore. Additionally, a timepoint and genotype
may be provided within the sample name too, separated by underscores. The formats are listed below:
    * geno_treatment_timepoint_replicate
    * treatment_timepoint_replicate => **(default)**
    * treatment_replicate
Note: Replicate numbers must not be provided within the groups_file, but the the treatment, treatment_timepoint or
geno_treatment_timepoint name must otherwise be identical to the sample names in the transcriptome.
* --sizes-genes / -s: Two-column CSV with the gene lengths of each gene in the transcript.

#### OPTIONAL ARGUMENTS
* -p / --PValue: Maximum P-Value to consider a gene "differentially expressed". Default: 0.05
* -l / --logFC: Min log Fold Change to consider a gene "differentially expressed". Default: 1
* -a / --adjust: Multiple comparison adjustment method (default bonferroni).
* -m / --mode: Choices: [up, down, any].  Allows to generate results for up/downregulated genes only. Default: any

#### OPTIONAL AESTHETICS ARGUMENTS
To color some genes differently within the volcano plots generated by this script, one may provide a list of locus_tags in
a txt file (one gene per line). Two distinct lists may be provided and the only difference is the color of the dots in
the volcano plot.

*  -b / --bio: Genes in this list will be colored blue.
*  -i / --interest: Genes in this list will be colored wine.

#### OUTPUT
The output is of this script is summarized within a file in the output_folder called "summary.significantGenes.txt".
This file lists:

* Total number of genes in the analysis.
* Total number of differentially expressed genes in at least one of the comparison groups.
* Total number of non-differentially expressed genes in any of the comparison groups.
* Results of each of the comparison groups. For each group, the following is listed in columns:
    * Treatment name.
    * Control name.
    * DEG: Number of differentially expressed genes.
    * DEG%: Percentage of differentially expressed genes.
    * DEBG: Number of differentially expressed genes that are in the "bio" list of genes.
    * DEBG%: Percentage of differentially expressed that are in the "bio" list of genes.

In addition to this, a subfolder will be generated for each comparison group, with each having a number of files:

* all.csv: Dataframe generated by the edgeR workflow. For reference about the meaning of each column, see the [edgeR documentation](https://bioconductor.org/packages/release/bioc/html/edgeR.html).
* significantGenes.txt: List of genes differentially expressed in this comparison group (one per line).
* MA.png: MA plot.
* volcano.png: Volcano plot.
