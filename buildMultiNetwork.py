#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
import os
import sys
import pandas as pd
import networkx as nx
import itertools as it
import numpy as np
import networkTools as nt
import argparse


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_files',
                        nargs='+')
    parser.add_argument('output_folder')
    parser.add_argument('--bait', '-b',
                        dest='bait_file',
                        required=True,
                        help='List of bait nodes (one per line).')
    parser.add_argument('--orthology', '-o',
                        dest='ortho_file',
                        required=True,
                        help='Orthology dictionary. 2-column CSV file with header.')
    parser.add_argument('--gene-info', '-g',
                        dest='info_file',
                        required=False,
                        help='Optional. 3-column CSV with header: Gene,plant,alias')
    parser.add_argument('--edge-type', '-e',
                        dest='edge_type_restriction',
                        required=False,
                        action='store_true',
                        default=False,
                        help='Flag. LC/OC will be built only with edges of one type')
    parser.add_argument('--exclusive', '-x',
                        dest='exclusive_community',
                        required=False,
                        action='store_true',
                        default=False,
                        help='Flag. LC/OC edge-lists will be built ONLY with members.')
    parser.add_argument('--pd_graph',
                        dest='pd_graph',
                        required=False,
                        action='store_true',
                        default=False,
                        help='Adv. Builds graph through pandas Frame. Only works in networkx dev version (git).')
    parser.add_argument('--etypes',
                        dest='etypes',
                        required=False,
                        default='',
                        help='Adv. Specify edge types for input files. Commma-separated. (Mandatory if using the '
                             'edge-type flag.')
    parser.add_argument('--untargeted',
                        required=False,
                        action='store_true',
                        default=False,
                        help='Flag.')
    parser.add_argument('--organisms',
                        required=True,
                        default='')
    parser.add_argument('--lc',
                        required=False,
                        choices=['none', 'multi', 'uni', 'all'],
                        default='none',
                        help='LC output options.')
    parser.add_argument('--verbose', '-v',
                        default=False,
                        action='store_true')
    return parser.parse_args()


def sort_strings(x):
    """
    Sorts list of strings of numbers by int value, and returns as list of strings.
    
    Args:
        x: List of strings.

    Returns:
        list: Sorted list of strings.
    """
    y = map(int, x)
    y = sorted(y)
    z = map(str, y)
    return z


def get_cliques(ortho_df, bait_dict):
    """
    Parses DataFrames to Graph and returns the dict of ALL cliques of each bait.
    
    Args:
        bait_dict:
        ortho_df (pd.DataFrame):

    Returns:
        (pd.Graph, list)
    """
    g = nt.df_to_graph(ortho_df, Options.pd_graph)

    # Add bait_nodes without ortho to ortho_g as isolates (dge conditions, for example)
    g.add_nodes_from(bait_dict)

    return g, list(nx.enumerate_all_cliques(g))


def filter_edges(G, nodes, match_this_set):
    """
    Mysterious edge filtering via generators that look simple.

    Args:
        G (nx.MultiGraph):
        nodes (list): For exclusive filtering of nodes. If empty, no node filtering.
        match_this_set (set):

    Returns:
        generator: Filtered edges.
    """
    #########################################
    # #      GENERATOR - DO NOT TOUCH       #
    if nodes:
        H = G.subgraph(nodes)
    else:
        H = G
    orig_edges = H.edges(keys=True, data='type')                    # Generator
    filtered_edges = ((u, v, k) for (u, v, k, t) in orig_edges if t in match_this_set)  # Updated generator
    #########################################
    return filtered_edges


def subgraph_nodes_type(g, n, t):
    """
    Subgraphs with type.

    Args:
        g (nx.MultiGraph):
        n (iterable): Nodes
        t (set): Edge types

    Returns:
        nx.MultiGraph: Subgraph
    """
    e = filter_edges(g, n, t)
    sg = g.edge_subgraph(e)
    unfronzen_sg = nx.MultiGraph(sg)
    unfronzen_sg.remove_nodes_from(nx.isolates(unfronzen_sg))

    return unfronzen_sg


def dict_to_midf(d):
    """
    Converts a dictionary to a multi-index dataframe.
    
    Args:
        d (dict): {etype: {tag: {gene1: T/None ...

    Returns:
        pd.DataFrame: Genes as index, etype_community at columns
    """
    midf = pd.DataFrame.from_dict({(i, j): d[i][j]
                                   for i in d.keys()
                                   for j in d[i].keys()},
                                  orient='columns')
    return midf


def midf_to_df(midf):
    """
    Converts multi-index dataframe to single-index dataframe.
    Args:
        midf (pd.DataFrame): 

    Returns:
        pd.DataFrame
    """
    df = midf
    if not len(df) == 0:
        header = midf.columns.droplevel(1) + '_' + midf.columns.droplevel(0)
        df.columns = header
    return df


def output_communities(d, name):
    """
    Process the results (dictionaries) into csv. Returns counts dataframe.
    
    Args:
        d (dict): results
        name (list[string, string]): LC/OC, tag/set
    
    Returns:
        pd.DataFrame
    """
    midf = dict_to_midf(d)
    df = midf_to_df(midf)

    # Filter out useless columns
    keep = []
    for cur_community in df.columns:
        if Options.lc_and in cur_community:
            n_tags = cur_community.count(Options.lc_and) + 1
        elif Options.oc_and in cur_community:
            n_tags = cur_community.count(Options.oc_and) + 1
        else:
            n_tags = 1

        vals = df[cur_community].value_counts()
        if True in vals:
            n_trues = vals[True]
        else:
            n_trues = 0

        if n_trues > n_tags:
            keep.append(cur_community)

    df = df[keep]

    # Converting booleans
    d = {True: 'TRUE', False: None}
    df = df.applymap(lambda x: d[x])

    fname = os.path.join(Options.output_folder, name[0], name[1])
    nt.check_output_folder_custom(os.path.join(Options.output_folder, name[0]))
    df.to_csv(fname, sep=',', index=True)

    counts_df = df.count().reset_index()
    counts_df.columns = ['code', 'count']

    fname = os.path.join(Options.output_folder, name[0], 'summary.csv')
    counts_df.to_csv(fname, sep=',', index=False)

    return counts_df


def identify_lc(g, bait_dict):
    """
    Identifies logic communities, and outputs to file immediately. Returns counts dataframe.
    
    Args:
        g (nx.MultiGraph):
        bait_dict (dict): 
    
    Returns:
        pd.DataFrame
    """
    all_nodes = set(g)
    lc_dict = {}                                                    # {root: {1 {gene1:'TRUE',gene2:None} / True, False?
    for cur_etype in Options.etypes:
        lc_dict[cur_etype] = {}
        g_etype = subgraph_nodes_type(g, [], {cur_etype})

        for cur_bait in bait_dict:
            if cur_bait not in g_etype:
                continue
            cur_bait_tag = bait_dict[cur_bait]
            lc_dict[cur_etype][cur_bait_tag] = {}

            # SINGLE LC AT A A TIME
            cur_nodes = set(nx.all_neighbors(g_etype, cur_bait))
            cur_nodes.update([cur_bait])

            if len(cur_nodes) == 1:                                 # useless network
                continue

            # OUTPUT
            if Options.lc in ['all', 'uni']:
                cur_subgraph = g_etype.subgraph(cur_nodes)
                cur_outfolder = nt.check_output_folder(os.path.join(Options.output_folder, 'LC'),
                                                       cur_bait_tag, cur_etype)
                nt.write_graph_output(cur_subgraph, Options.etypes, [], cur_outfolder)

            # TAGGING
            for n in cur_nodes:
                lc_dict[cur_etype][cur_bait_tag][n] = True

            for cur_not_node in all_nodes.difference(cur_nodes):
                lc_dict[cur_etype][cur_bait_tag][cur_not_node] = False

        # MULTI-LC
        if Options.lc in ['all', 'multi']:
            etype_df = pd.DataFrame(lc_dict[cur_etype])

            available_tags = lc_dict[cur_etype].keys()
            available_tags = sorted(available_tags)
            tags_to_check = [n for m in range(2, len(available_tags) + 1) for n in it.combinations(available_tags, m)]

            for cur_bait_tags in tags_to_check:
                merged_tag = Options.lc_and.join(sort_strings(cur_bait_tags))

                gene_col = etype_df[cur_bait_tags[0]]                               # inititalize with first tag
                for cur_bait in cur_bait_tags[1:]:                                  # from second to end
                    gene_col = gene_col & etype_df[cur_bait]

                if np.all(~gene_col):                                       # empty tag
                    continue
                else:
                    lc_dict[cur_etype][merged_tag] = gene_col.to_dict()             # this makes it be part of summary
                    cur_nodes = gene_col[gene_col].index.tolist()
                    cur_subgraph = g_etype.subgraph(cur_nodes)

                    # OUTPUT
                    cur_outfolder = nt.check_output_folder(os.path.join(Options.output_folder, 'LC'), merged_tag,
                                                           cur_etype)
                    nt.write_graph_output(cur_subgraph, Options.etypes, [], cur_outfolder)

    counts = output_communities(lc_dict, ['LC', 'logic_communities.csv'])

    return counts


def identify_oc(g, bait_dict, ortho_df):
    """
    Identifies orthologous communities, and outputs to file immediately. Returns counts dataframe.
    
    Args:
        g (nx.MultiGraph):
        bait_dict (dict): 
        ortho_df (pd.DatFrame):
    
    Returns:
        pd.DataFrame.
    """
    all_nodes = set(g)
    oc_dict = {}                                                # {root {1 {gene1:'TRUE',gene2:None}

    ortho_g, cliques_list = get_cliques(ortho_df, bait_dict)
    # ^^ orthocliques: subgraphs of fully interconnected nodes.

    for cur_etype in Options.etypes:
        oc_dict[cur_etype] = {}
        g_etype = subgraph_nodes_type(g, [], {cur_etype})

        for cur_bait in bait_dict:
            if cur_bait not in g_etype:
                continue

            # We find ortho_cliques: i.e. all fully interconnected groups of orthologous genes. Pairs if 1-to-1.
            # Specifically, bait ortho_cliques. These cliques are our new bait.
            cur_baits_cliques = nx.cliques_containing_node(ortho_g, nodes=cur_bait, cliques=cliques_list)
            n_cliques = len(cur_baits_cliques)
            if n_cliques == 0:                     # No cliques
                continue

            for cur_clique in cur_baits_cliques:
                n_tags = len(cur_clique)

                nodes_in_g = []
                nodes_in_bait = []
                for cur_node in cur_clique:
                    nodes_in_g.append(g_etype.has_node(cur_node))
                    nodes_in_bait.append(cur_node in bait_dict)

                if not np.all(nodes_in_g):                      # we want all bait_ortho_clique nodes in the graph
                    continue
                if not np.all(nodes_in_bait):                   # we want all bait_ortho_clique nodes in the bait
                    continue

                tags = [bait_dict[n] for n in cur_clique]
                merged_tag = Options.oc_and.join(sort_strings(tags))

                if merged_tag in oc_dict[cur_etype]:            # if this merged_tag has been done already, continue
                    continue
                else:                                           # else, initialize
                    oc_dict[cur_etype][merged_tag] = {}

                # Now we identify the neighbors of each bait_ortho_clique node in the other networks (coexpression)
                cur_neighbors = set()
                for cur_node in cur_clique:
                    cur_neighbors.update(set(nx.all_neighbors(g_etype, cur_node)))
                    cur_neighbors.update([cur_node])

                ortho_subgraph = subgraph_nodes_type(g, cur_neighbors, {'Orthology'})
                ortho_subgraph_cliques = nx.cliques_containing_node(ortho_subgraph, nodes=list(cur_neighbors))

                nodes_to_tag = set()
                for cur_node in ortho_subgraph_cliques:
                    if not ortho_subgraph_cliques[cur_node]:
                        continue
                    # (below) every node must be a maximum clique.
                    # do not compare with total organisms because we want groups with 2/3 orgs, for examples
                    # will be > n_tags if the bait is without ortho (such as dge).
                    if len(ortho_subgraph_cliques[cur_node][0]) >= n_tags:
                        nodes_to_tag.update([cur_node])               # this should have ONLY OC members

                if len(nodes_to_tag) <= n_tags:                 # useless network
                    continue

                # add bait to nodes_to_tag
                if n_tags == 1:
                    nodes_to_tag.add(cur_bait)

                if Options.exclusive_community:
                    cur_subgraph = subgraph_nodes_type(g, nodes_to_tag, {cur_etype, 'Orthology'})
                else:
                    cur_subgraph = subgraph_nodes_type(g, cur_neighbors, {cur_etype, 'Orthology'})

                # OUTPUT
                cur_outfolder = nt.check_output_folder(os.path.join(Options.output_folder, 'OC'), merged_tag, cur_etype)
                nt.write_graph_output(cur_subgraph, Options.etypes, [], cur_outfolder)

                # TAGGING
                for n in nodes_to_tag:
                    oc_dict[cur_etype][merged_tag][n] = True

                for cur_not_node in all_nodes.difference(nodes_to_tag):
                    oc_dict[cur_etype][merged_tag][cur_not_node] = False

    counts = output_communities(oc_dict, ['OC', 'orthologous_communities.csv'])

    return counts


def ego_networks(all_edges, bait_list):
    """
    Gets half-ego and ego networks.
    
    Args:
        all_edges (pd.DataFrame):
        bait_list (list):

    Returns:
        (pd.DataFrame, pd.DataFrame): Ego and half-ego edges.
    """
    half_edges_coexp = all_edges[((all_edges['source'].isin(bait_list) | all_edges['target'].isin(bait_list)) &
                                  ~all_edges['type'].isin(['Orthology'])
                                  )]

    half_nodes = nt.get_nodes_from_df(half_edges_coexp)
    half_nodes.update(bait_list)                       # in case some baits don't have coexp.

    half_edges_orth = all_edges[(all_edges['source'].isin(half_nodes) &
                                 all_edges['target'].isin(half_nodes) &
                                 all_edges['type'].isin(['Orthology'])
                                 )]

    half_edges = pd.concat([half_edges_coexp, half_edges_orth]).reset_index(drop=True)

    ego_edges = all_edges[all_edges['source'].isin(half_nodes) &
                          all_edges['target'].isin(half_nodes)
                          ].reset_index(drop=True)

    return ego_edges, half_edges


def human_summary(results, bait_list):
    """
    Generates a human-readable summary.
    
    Args:
        results (pd.DataFrame):
        bait_list (list): 

    Returns:
        pd.DataFrame
    """
    gene_info = pd.read_csv(Options.info_file, index_col=0, header=0)

    c_types = []
    edges = []
    plants = []
    aliases = []
    baits = []
    counts = []
    codes = []
    for cur_index in range(len(results.index)):
        cur_edge, cur_code = results.code[cur_index].split('_')

        edges.append(cur_edge)
        codes.append(cur_code)
        counts.append(results['count'][cur_index])

        if Options.oc_and in cur_code:
            c_types.append('OC')
            cur_bait = [bait_list[int(n)] for n in cur_code.split(Options.oc_and)]
        else:
            c_types.append('LC')
            cur_bait = [bait_list[int(n)] for n in cur_code.split(Options.lc_and)]

        plants.append(';'.join(set([gene_info.plant[n] for n in cur_bait if n in gene_info.plant])))
        aliases.append(';'.join(set([gene_info.alias[n] for n in cur_bait if n in gene_info.alias])))

        baits.append(';'.join(cur_bait))

    summary = pd.DataFrame({'c_type': c_types,
                            'edge': edges,
                            'plant': plants,
                            'alias': aliases,
                            'bait': baits,
                            'count': counts,
                            'code': codes
                            })

    fname = os.path.join(Options.output_folder, 'full_summary.csv')
    summary.to_csv(fname, sep=',', index=False)

    return summary


#################
# MAIN PIPELINE #
#################
def main():
    # PARSE INPUT
    if not Options.ortho_file:
        print('Orthology is necessary.')
        sys.exit()
    if Options.edge_type_restriction and not Options.etypes:
        print('For now, edge_types_restriction needs etypes.')
        sys.exit()

    # OUTPUT FOLDER CREATION
    if not os.path.exists(Options.output_folder):
        os.makedirs(Options.output_folder)
        logfile = open(os.path.join(Options.output_folder, 'logfile.txt'), 'w')
        logfile.write(' '.join(sys.argv))
        logfile.close()

    # LOAD INPUT
    all_edges, gene_org_dict = nt.load_edges(Options.input_files, Options.etypes, Options.organisms, Options.verbose)

    # ORTHOLOGY
    if Options.verbose:
        print('Loading orthology')
    Options.ortho_file = [Options.ortho_file]
    ortho_df, gene_org_dict2 = nt.get_orthology(Options.ortho_file, set())
    gene_org_dict.update(gene_org_dict2)
    del gene_org_dict2

    # TARGETED
    if Options.bait_file:
        # # EGO
        if Options.verbose:
            print('Egoing around baits')

        # PARSE BAIT
        bait_dict = {}      # for OC/LC code purposes.
        bait_list = []
        with open(Options.bait_file) as f:
            for i, line in enumerate(f):
                cur_bait = line.rstrip()
                bait_dict[cur_bait] = str(i)
                bait_list.append(cur_bait)

        # BAIT HAS ORTH?

        ego_edges, half_edges = ego_networks(all_edges, bait_list)
        ego_nodes = nt.get_nodes_from_df(ego_edges)

        # COEXPRESSION + ORTHOLOGY MERGE
        ortho_df_ego_nodes = nt.filter_df(ortho_df, ego_nodes)
        all_edges = pd.concat([all_edges, ortho_df_ego_nodes], sort=True).reset_index(drop=True)
        ego_edges = pd.concat([ego_edges, ortho_df_ego_nodes], sort=True).reset_index(drop=True)
        half_edges = pd.concat([half_edges, ortho_df_ego_nodes], sort=True).reset_index(drop=True)

        # NETWORK OUTPUT
        if Options.verbose:
            print('Writing full, ego, and half-ego networks')
        nt.write_graph_df_output(all_edges, Options.etypes, [], Options.output_folder)

        cur_outfolder = nt.check_output_folder(Options.output_folder, 'half_ego', edge_type='')
        nt.write_graph_df_output(half_edges, Options.etypes, [], cur_outfolder)
        del half_edges

        cur_outfolder = nt.check_output_folder(Options.output_folder, 'ego', edge_type='')
        nt.write_graph_df_output(ego_edges, Options.etypes, [], cur_outfolder)

        # # DATAFRAME TO NETWORK
        if Options.verbose:
            print('Converting to networkx graph')
        network_graph = nt.df_to_graph(ego_edges, Options.pd_graph)

        results = []

        # # LC IDENTIFICATION
        if not Options.lc == 'none':
            if Options.verbose:
                print('Identifying LCs')
            results.append(identify_lc(network_graph, bait_dict))

        # # OC IDENTIFICATION
        if Options.ortho_file:
            if Options.verbose:
                print('Identifying OCs')
            results.append(identify_oc(network_graph, bait_dict, ortho_df_ego_nodes))

        # PRETTY-SUMMARIZING
        if Options.info_file:
            if Options.verbose:
                print('Summarizing')
            if len(results) == 1:
                results = results[0]
            else:
                results = pd.concat(results).reset_index(drop=True)

            human_summary(results, bait_list)

    # UNTARGETED
    nt.check_output_folder_custom(os.path.join(Options.output_folder, 'untargeted/'))

    if Options.untargeted or not Options.bait_file:
        # # ORTHOLOGY NETWORK
        ortho_g = nt.df_to_graph(ortho_df, Options.pd_graph)
        del ortho_df

        # # FAMILY NAME AND CODE DEFINING
        fam_dict, fam_code_dict = nt.process_family(ortho_g, gene_org_dict, Options.organisms)
        del ortho_g

        # WRITE FAMILY NAME DICTIONARY
        with open(os.path.join(Options.output_folder, 'untargeted', 'fam_dict.csv'), 'w') as f:
            for k in fam_dict:
                line = ','.join([k, fam_dict[k]])
                f.write(line)
                f.write('\n')

        # COEXPRESSION + FAMILY MERGE
        new_sources = all_edges.source.map(fam_dict)
        new_targets = all_edges.target.map(fam_dict)

        new_edges = pd.DataFrame(data={'source': new_sources,
                                       'target': new_targets,
                                       'value': all_edges.value,
                                       'file': all_edges.file,
                                       'organism': all_edges.organism}).dropna().reset_index(drop=True)
        del all_edges

        if Options.verbose:
            print('Writing complex')
        nt.write_graph_df_output_custom(new_edges, ['value', 'file', 'organism'],
                                        os.path.join(Options.output_folder, 'untargeted/complex/'))

        if Options.verbose:
            print('Graphing')
        G = nt.df_to_graph(new_edges, Options.pd_graph)
        del new_edges

        # SCORING NETWORK
        if Options.verbose:
            print('Scoring family network')
        real_edge_score, real_edge_orgs = nt.score_fam_network(G)
        del G

        # BUILDING SIMPLE NETWORK, WITH COMPARED SCORES
        simple_g = nx.Graph()
        for cur_edge in real_edge_score:
            source_fam, target_fam = cur_edge

            if source_fam == target_fam:
                continue

            # Divisions by zero may occurr if typos when writing species....
            simple_score = real_edge_score[cur_edge] / nt.expected_score(source_fam, target_fam, fam_code_dict,
                                                                         Options.organisms)
            simple_orgs = len(real_edge_orgs[cur_edge]) / len(Options.organisms)

            if simple_score > 0:
                simple_g.add_edge(source_fam, target_fam,
                                  n_edges=real_edge_score[cur_edge],
                                  value=simple_score,
                                  organisms=simple_orgs)

        # WRITING
        if Options.verbose:
            print('Writing simple')
        nt.write_graph_output_custom(simple_g, ['n_edges', 'value', 'organisms'],
                                     os.path.join(Options.output_folder, 'untargeted/simple/'))

    return


if __name__ == "__main__":
    Options = get_args()
    Options.organisms = Options.organisms.split(',')
    Options.etypes = Options.etypes.split(',')
    Options.lc_and = '^'
    Options.oc_and = 'o'
    main()
